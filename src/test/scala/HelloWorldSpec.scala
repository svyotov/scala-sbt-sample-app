import org.scalatest._

class HelloWorldSpec extends FlatSpec with Matchers {
  "double" should "double a number" in {
    HelloWorld.double(21) should === (42)
  }
}

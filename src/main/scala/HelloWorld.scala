object HelloWorld {
  def main(args: Array[String]): Unit = {
    println("Hello world!")
  }

  // A simple function we can easily unit test
  def double(x: Int): Int = x + x
}
